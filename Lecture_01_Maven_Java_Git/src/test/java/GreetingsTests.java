import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson1.Greetings;
import ru.edu.lesson1.IGreeting;

public class GreetingsTests {

    private final IGreeting greeting = new Greetings();

    @Test
    public void testFirstName() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals("Olga", greeting.getFirstName());
    }

    @Test
    public void testSecondName() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals("Alekseevna", greeting.getSecondName());
    }

//    My name is Khromova Olga Alekseevna.
//    My hobbies: Antigravity (aerial acobatics), Argentine tango (social dance).
//    Phone number: 84951234567.
//    BitBucket URL: .
//    Course expectations: .
//    Education: .

    @Test
    public void testLastName() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals("Khromova", greeting.getLastName());
    }

    @Test
    public void testHobbies() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals(2, greeting.getHobbies().size());
    }

    @Test
    public void testPrintHobbies() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals("Antigravity (aerial acrobatics), Argentine tango (social dance)", ((Greetings)greeting).printHobbies());
    }

    @Test
    public void testPhone() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals("84951234567", greeting.getPhone());
    }

    @Test
    public void testBitBucket() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals("https://bitbucket.org/olyka/", greeting.getBitbucketUrl());
    }

    @Test
    public void testExpectations() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals("I want to become Java Developer and start to program (again)", greeting.getCourseExpectation());
    }

    @Test
    public void testEducation() {
        Assert.assertNotNull(greeting);
        Assert.assertEquals("Moscow Institute of Electronics and Mathematics, Faculty of Applied Mathematics, Department of Mathematical and Software for Information Processing and Control Systems", greeting.getEducationInfo());
    }

}
