package ru.edu.lesson1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Greetings implements IGreeting {

    private List<Hobby> hobbies = new ArrayList<>();

    private String firstName = "Olga";
    private String secondName = "Alekseevna";
    private String lastName = "Khromova";
    private String phone = "84951234567";
    private String bitBucket = "https://bitbucket.org/olyka/";
    private String expectations = "I want to become Java Developer and start to program (again)";
    private String education = ("Moscow Institute of Electronics and Mathematics, " +
            "Faculty of Applied Mathematics, " +
            "Department of Mathematical and Software for Information Processing and Control Systems");

    public Greetings() {
        hobbies.add(new Hobby("1", "Antigravity", "aerial acrobatics"));
        hobbies.add(new Hobby("2", "Argentine tango", "social dance"));
    }

    /**
     * Get first name.
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get second name.
     */
    @Override
    public String getSecondName() {
        return secondName;
    }

    /**
     * Get last name.
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Get hobbies.
     */
    @Override
    public Collection<Hobby> getHobbies() {
        return hobbies;
    }

    /**
     * Print all hobbies in line.
     */
    public String printHobbies() {
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < hobbies.size(); i++) {
            temp.append(hobbies.get(i));
            if (i < (hobbies.size() - 1)) temp.append(", ");
        }
        return temp.toString();
    }

    /**
     * Get bitbucket url to your repo.
     */
    @Override
    public String getBitbucketUrl() {
        return bitBucket;
    }

    /**
     * Get phone number.
     */
    @Override
    public String getPhone() {
        return phone;
    }

    /**
     * Your expectations about course.
     */
    @Override
    public String getCourseExpectation() {
        return expectations;
    }

    /**
     * Print your university and faculty here.
     */
    @Override
    public String getEducationInfo() {
        return education;
    }

    @Override
    public String toString() {
        return "About me:\n" +
                "My name is " + lastName + " " + firstName + " " + secondName + ".\n" +
                "My hobbies: " + printHobbies() + ".\n" +
                "Phone number: " + phone + ".\n" +
                "BitBucket URL: " + bitBucket + ".\n" +
                "Course expectations: " + expectations + ".\n" +
                "Education: " + education + ".\n";
    }

    public static void main(String[] args) {
        Greetings greeting = new Greetings();
        System.out.println(greeting);
    }

}
