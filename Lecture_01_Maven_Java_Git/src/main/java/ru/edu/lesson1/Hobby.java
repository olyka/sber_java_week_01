package ru.edu.lesson1;

/**
 * Hobby class
 */
public class Hobby {

    private final String id;
    private final String name;
    private final String description;

    public Hobby(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Hobby(String id, String name) {
        this(id, name, "");
    }

    /**
     * Get hobby's ID
     */
    public String getId() {
        return id;
    }

    /**
     * Get hobby's name
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        if (description.length() == 0) {
            return name;
        }
        return name + " (" + description + ")";
    }

}
