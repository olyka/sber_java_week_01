package ru.edu.lesson1;

public class MyDigitConverter implements DigitConverter, DigitConverterHard {

    /**
     * Преобразование целого десятичного числа в другую систему счисления
     *
     * @param digit - число
     * @param radix - основание системы счисления
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    @Override
    public String convert(int digit, int radix) {

        if (digit == 0) {
            return "0";
        }

        if (digit < 0) {
            throw new IllegalArgumentException("Your digit is negative. It has to be positive or 0.");
        }

        if (radix < 1) {
            throw new IllegalArgumentException("You radix has to be larger than 1.");
        }

        int temp = digit;
        StringBuilder res = new StringBuilder();

        while (temp > 0) {
            res.insert(0, temp % radix);
            temp /= radix;
        }

        return res.toString();

    }

    /**
     * Преобразование вещественного десятичного числа в другую систему счисления
     *
     * @param digit     - число
     * @param radix     - основание системы счисления
     * @param precision - точность, сколько знаков после '.'
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    @Override
    public String convert(double digit, int radix, int precision) {
        return null;
    }

    public static void main(String[] args) {
        MyDigitConverter converter = new MyDigitConverter();
//        System.out.println(converter.convert(-1, 2));
//        System.out.println(converter.convert(2, 0));
        System.out.println(converter.convert(2, 2));
    }

}
