package ru.edu.lesson1;

/**
 * Конвертор десятичных чисел в другие системы счисления
 */
public interface DigitConverter {

    /**
     * Преобразование целого десятичного числа в другую систему счисления
     *
     * @param digit - число
     * @param radix - основание системы счисления
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    String convert(int digit, int radix);



}
